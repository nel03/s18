// console.log('hello');

/*
	function able to receive data without the use of global variable or promt()
*/

/*
	name - is a parameter

	parameter
		> is a variable/ container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
*/
function printName(name){
	console.log("My name is " + name);
};


/*
	data passed into a function invocation can be received by the function

	this is what we call an argument
*/
printName("Nel");
printName('Seven');


/*
	data passed into the function through function invocation is called arguments

	the arguments is then stored within a container called a parameter
*/
function printMyAge(age){
	console.log('I am ' + age);
};

printMyAge(19);
printMyAge();

/*
	check divisibility reusably using a function with arguments and parameter
*/

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log('The remainder of ' + num + " divided by 8 is: " + remainder);
	
	let isDivisibleBy8 = remainder === 0;
	console.log('Is ' + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

/*
	mini Activity
*/

function superName(name){
	console.log("I'am " + name + " Best drink ever");
};

superName('Gin');

function printIdNumber(number){
	let evenNum = number % 6;
	console.log('is this number a ' + number + " even number?");

	let theNumber = evenNum == 0;
	console.log('is ' + number + ' even number');
	console.log(theNumber);
};

printIdNumber(64);
printIdNumber(66);

/*
	multiple arguments can also be passed into a function

	multiple parameters can contain our arguments
*/

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial + ' ' + lastName);
}

printFullName('Juan', 'Crisostomo', 'Ibarra');
printFullName('Ibarra', 'Juan', 'Crisostomo');

/*
	parameters will contain the argument according to the order it was passed

	in other language, providing more/less arguments than the expected parameters sometime causes an error or changes the behavior of the function
*/

printFullName('Stephen', ' ', 'wardell');
printFullName('Stephen', 'wardell');

/*
	use variables as arguments
*/

let fName = 'lary';
let mName = 'Joe';
let lName = 'bird';

printFullName(fName, mName, lName);

/*
	mini activity
*/

function favDrinks(sOne, sTwo, sThree, sFour, sFive){
	console.log('The best drinks today are:')
	console.log(sOne + ' ' + sTwo + ' ' + sThree + ' ' + sFour + ' ' + sFive);

	// return sOne + sTwo + sThree + sFour + sFive;
	// return "Choose your Drink";
};
	
favDrinks('Red Wine', 'Beer', ' Gin', 'Empe', 'whisky');

/*
	Return Statement
		>currently or so far, our function are able to display data in our console
		>however our function cannot yet return values. function are abel to return values which can be saved into a variable using the return statement/ keyword

*/

let fullName = printFullName('Gin', 'empe', 'whisky');
console.log(fullName);

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
}

fullName = returnFullName('wine', 'Gin', 'Red Horse');
// printFullaName(); return undefined because the function does not have a return statement.
//returnFullName(); return a string as a value because it has a return statement


console.log(fullName);
/*
	function which have a return statement are able to return value and it can be saved in a variable
*/
console.log(fullName + ' are my drinks');

function returnPhilippineAddress(city){
	return city + " , Philippines.";
};

/*
	return - return a values from a function which we can save in a variable
*/

let myFullAddress = returnPhilippineAddress('San Pedro');
console.log(myFullAddress);

/*
	returns true if number is divisible by 4, else returns false
*/

function checkDivisibilityBy4(number){
	let remainder = number % 4;
	let isDivisibleBy4 = remainder === 0;

	/*
		returns either true or false
		not only can you return raw values/data, you can also directly return a value
	*/

	return isDivisibleBy4;

	/*
		return keyword not only allows us to return value but also ends the process of the function
	*/
	console.log('i am run after the return.');
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);
console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

/*
	mini-activity
*/

function drinksToday(drink1, drink2){
	// let drink1 = 46;
	// let drink2 = 53;
	// let sum = drink1 + drink2;

	return drink1 + drink2;
}

let soldDrinks = drinksToday(46,53);
console.log('Sales for Today: ')
console.log(soldDrinks);



let totalSales
function bulkSales(sale1, sale2){
	return sale1 * sale2;
}

totalSales = bulkSales(5,13);
console.log('Monday Total Sales: ');
console.log(totalSales);



function pieCircle(radius){
	return (Math.PI*radius*radius);
};

let theCircle = pieCircle(6)
console.log(theCircle);

// or

function getCircleArea(radius){
	// ** exponent operator ^
	/*
		A= pi r ^2
	*/

	// let area = 3.1416 * (radius**2);
	// return area;
	// or
	return 3.1416 * (radius**2);
};

let circleArea = getCircleArea(15);
console.log('the result of getting the area of a cicle: ');
console.log(circleArea);

/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

function printPassingScore(score,tScore){
	let myScore = (score/tScore)*100; 
	let isPassed = myScore > 75;
	// console.log(myScore);
	return isPassed;
	
}
let overAllScore = printPassingScore(79,90);
console.log('Did your Score Passed?')
console.log(overAllScore);